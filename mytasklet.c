#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>			// kmalloc
#include <linux/proc_fs.h>		// create_proc
#include <linux/seq_file.h>		// sequence file ops
#include <linux/interrupt.h>	// работа с тасклетами

static struct proc_dir_entry *mytasklet_proc_file;

/*
struct tasklet_struct {
	struct tasklet_struct *next; // указатель на следующий тасклет в списке
	unsigned long state;         // текущее состояние тасклета
	atomic_t count;              // счетчик ссылок {0 - тасклет разрешен к исполнению}
	void (*func)(unsigned long); // функция-обработчик тасклета
	unsigned long data;          // аргумент функции-обработчика тасклета
};
*/
static struct tasklet_struct *mytasklet_struct;

static int mytasklet_proc_show(struct seq_file *m, void *v)
{
	mytasklet_struct->data++;
	seq_printf(m, "Scheduling mytasklet\n");
	tasklet_schedule(mytasklet_struct);
	seq_printf(m, "mytasklet_module: state is %lu, count is %d, data is %lu\n",
		mytasklet_struct->state,
		atomic_read(&mytasklet_struct->count),
		mytasklet_struct->data);
	return 0;
}

static int tasklet_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, mytasklet_proc_show, NULL);
}

static const struct file_operations mytasklet_proc_fops = {
	.open = tasklet_proc_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

static void mytasklet_function(unsigned long arg)
{
	printk(KERN_INFO "mytasklet_module: Now in mytasklet_function.\n");
	printk(KERN_INFO "mytasklet_module: state is %lu, count is %d, data is %lu\n",
		mytasklet_struct->state,
		atomic_read(&mytasklet_struct->count),
		mytasklet_struct->data);
}

static int __init mytasklet_module_init(void)
{
	printk(KERN_INFO "mytasklet_module: Loading module...\n");

	mytasklet_proc_file = proc_create("mytasklet", 0444, NULL, &mytasklet_proc_fops);
	printk(KERN_INFO "mytasklet_module: proc entry created.\n");

	mytasklet_struct = kmalloc(sizeof(struct tasklet_struct), GFP_KERNEL);
	tasklet_init(mytasklet_struct, mytasklet_function, 0);
	printk(KERN_INFO "mytasklet_module: takslet initialized.\n");

	printk(KERN_INFO "mytasklet_module: Loading complete.\n");
	return 0;
}

static void __exit mytasklet_module_exit(void)
{
	tasklet_kill(mytasklet_struct);
	printk(KERN_INFO "mytasklet_module: mytasklet killed.\n");
	proc_remove(mytasklet_proc_file);
	printk(KERN_INFO "mytasklet_module: proc entry removed.\n");
	printk(KERN_INFO "mytasklet_module: Module unloaded.\n");
}

module_init(mytasklet_module_init);
module_exit(mytasklet_module_exit);

